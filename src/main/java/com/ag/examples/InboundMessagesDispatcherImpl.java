package com.ag.examples;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class InboundMessagesDispatcherImpl implements InboundMessagesDispatcher {

    private ThreadPoolExecutor executor;

    public InboundMessagesDispatcherImpl(int threadsNumber) {
        this.executor = new ThreadPoolExecutor(threadsNumber, threadsNumber,
                1, TimeUnit.HOURS,
                new ArrayBlockingQueue<>(1000));
    }

    @Override
    public void process(MessageProcessingTask messageProcessingTask) {
        executor.execute(messageProcessingTask);
    }
}
