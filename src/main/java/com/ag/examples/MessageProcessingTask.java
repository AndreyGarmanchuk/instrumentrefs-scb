package com.ag.examples;

import com.ag.examples.merge.Merger;
import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.model.RawMessage;
import com.ag.examples.parser.MessageParcer;

import static com.ag.examples.utils.Logger.logError;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class MessageProcessingTask<T> implements Runnable {

    private RawMessage<T> message;
    private MessageParcer<T> parcer;
    private Merger merger;

    public MessageProcessingTask(RawMessage<T> message,
                                 MessageParcer<T> parcer,
                                 Merger merger) {
        this.message = message;
        this.parcer = parcer;
        this.merger = merger;
    }

    @Override
    public void run() {
        try {
            process();
        } catch (Exception e) {
            logError("Error while processing " + message, e);
        }
    }

    public void process() {
        InstrumentUpdate instrumentUpdate = null;
        try {
            // 1. Parse
            instrumentUpdate = parcer.parse(message);
        } catch (Exception e) {
            logError("Error while parsing " + message + "; check message structure and data");
            return;
        }
        // 2. Merge
        Instrument instrument = merger.merge(instrumentUpdate);
    }
}
