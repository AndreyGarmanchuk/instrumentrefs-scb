package com.ag.examples;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface InboundMessagesDispatcher<T> {

    void process(MessageProcessingTask processor);
}
