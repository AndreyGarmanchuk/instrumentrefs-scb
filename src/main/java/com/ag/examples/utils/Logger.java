package com.ag.examples.utils;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class Logger {

    public static void logInfo(String message) {
        System.out.println(message);
    }

    public static void logError(String message) {
        System.out.println("ERROR: " + message);
    }

    public static void logError(String message, Exception e) {
        System.out.println("ERROR: " + message);
        e.printStackTrace();
    }
}
