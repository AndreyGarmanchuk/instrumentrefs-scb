package com.ag.examples;

import com.ag.examples.dao.SimpleInstrumentStorageDaoImpl;
import com.ag.examples.merge.Merger;
import com.ag.examples.dao.InstrumentStorageDao;
import com.ag.examples.model.StringRawMessage;
import com.ag.examples.parser.CLIMessageParserImpl;
import com.ag.examples.policy.MergePolicyLoader;
import com.ag.examples.policy.MergePolicyRepo;
import com.ag.examples.policy.StandardMergePolicy;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Scanner;

import static com.ag.examples.utils.Logger.logInfo;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Main class for starting application.
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com).
 */
public class App {

    public static void main(String[] args) throws IOException, InterruptedException {
        int threadsNumber = getThreadsNumber(args);
        String policiesFolder = getPolicyFolder(args);

        /* ---------------------------------------------------------------------------------- */
        /* Application configuration (should me moved in some dependency inversion container) */
        CLIMessageParserImpl cliMessageParser = new CLIMessageParserImpl();
        InboundMessagesDispatcherImpl dispatcher = new InboundMessagesDispatcherImpl(threadsNumber);
        MergePolicyRepo mergePolicyRepo = new MergePolicyRepo();
        InstrumentStorageDao instrumentStorageDao = new SimpleInstrumentStorageDaoImpl();
        Merger merger = new Merger(mergePolicyRepo, instrumentStorageDao);

        /* Reference implementation of MergePolicy with rules in Java - replaced with LoadableMergePolicy with rules in extrenal source
            mergePolicyRepo.registerPolicy(new StandardMergePolicy());
        */
        MergePolicyLoader loader = new MergePolicyLoader(mergePolicyRepo, policiesFolder);
        loader.init();
        /* ---------------------------------------------------------------------------------- */

        logInfo("");
        logInfo("Send message via command: \n" + CLIMessageParserImpl.COMMAND_TEMPLATE);
        logInfo("Example: \n" + CLIMessageParserImpl.COMMAND_EXAMPLE);
        logInfo("\nTo quit: Q or QUIT or EXIT");
        logInfo("Enter command: ");

        Scanner in = new Scanner(System.in);
        String rawCommand = in.nextLine();
        while (true) {
            try {
                if (rawCommand.equalsIgnoreCase("exit") || rawCommand.equalsIgnoreCase("quit") || rawCommand.equalsIgnoreCase("q")) {
                    logInfo("Exiting...");
                    System.exit(0);
                }

                dispatcher.process(new MessageProcessingTask<>(new StringRawMessage(rawCommand), cliMessageParser, merger));

                Thread.sleep(1000);
                logInfo("---------------------------------------- current ref data ----------------------------------");
                logInfo(instrumentStorageDao.toString());
                logInfo("--------------------------------------------------------------------------------------------");

                Thread.sleep(1000);
                logInfo("Enter next command: ");

                rawCommand = in.nextLine();
            } catch (IllegalArgumentException e) {
                logInfo(e.getMessage());
                rawCommand = in.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
                rawCommand = in.nextLine();
            }
        }
    }

    private static int getThreadsNumber(String[] args) {
        int threadsNumber = 4;
        try {
            if (args != null && args.length > 0) {
                threadsNumber = Integer.parseInt(args[0]);
            } else {
                logInfo("Cannot parse fisrt parameter, use default value of number_of_processing_threads = " + threadsNumber);
            }
        } catch (NumberFormatException e) {
            logInfo("Cannot parse fisrt parameter, use default value of number_of_processing_threads = " + threadsNumber);
        }

        logInfo("cfg: number of processing threads " + threadsNumber);
        return threadsNumber;
    }

    private static String getPolicyFolder(String[] args) {
        String mergePoliciesFolder = "merge-policies";
        try {
            if (args != null && args.length > 1) {
                mergePoliciesFolder = args[1];
            } else {
                logInfo("Cannot parse second parameter, use default value of merge-policies folder = '" + mergePoliciesFolder + "'");
            }
        } catch (NumberFormatException e) {
            logInfo("Cannot parse second parameter, use default value of merge-policies folder = '" + mergePoliciesFolder + "'");
        }

        logInfo("cfg: reading policies from folder '" + mergePoliciesFolder + "'");
        return mergePoliciesFolder;
    }

}
