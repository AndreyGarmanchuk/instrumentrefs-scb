package com.ag.examples.model;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class StringRawMessage implements RawMessage<String> {

    private String message;

    public StringRawMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "message={" + message + '}';
    }
}
