package com.ag.examples.model;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class InstrumentUpdate {

    private String source, key;
    private Instrument instrument;

    public InstrumentUpdate(String source, String key, Instrument instrument) {
        this.source = source;
        this.key = key;
        this.instrument = instrument;
    }

    public String getSource() {
        return source;
    }

    public String getKey() {
        return key;
    }

    public Instrument getInstrumentUpdate() {
        return instrument;
    }

    @Override
    public String toString() {
        return "InstrumentUpdate{" +
                "source='" + source + '\'' +
                ", key='" + key + '\'' +
                ", instrument=" + instrument +
                '}';
    }
}
