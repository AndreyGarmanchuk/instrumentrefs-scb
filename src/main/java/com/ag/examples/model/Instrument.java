package com.ag.examples.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class Instrument extends ConcurrentHashMap<String, String> {

    public Instrument() {
        super();
    }

    public Instrument(Map<String, String> instrument) {
        putAll(instrument);
    }
}
