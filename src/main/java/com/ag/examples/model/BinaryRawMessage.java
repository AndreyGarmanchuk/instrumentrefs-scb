package com.ag.examples.model;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class BinaryRawMessage implements RawMessage<byte[]> {

    private byte[] message;

    public BinaryRawMessage(byte[] message) {
        this.message = message;
    }

    public byte[] getMessage() {
        return message;
    }
}
