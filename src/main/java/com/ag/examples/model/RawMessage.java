package com.ag.examples.model;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface RawMessage<T> {

    T getMessage();
}
