package com.ag.examples.parser;

import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.model.RawMessage;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class BinaryMessageParserImpl implements MessageParcer<byte[]> {

    @Override
    public InstrumentUpdate parse(RawMessage<byte[]> rawMessage) {
        throw new UnsupportedOperationException("BinaryMessageParserImpl is not implemented yet");
    }

}
