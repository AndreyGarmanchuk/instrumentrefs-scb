package com.ag.examples.parser;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.model.RawMessage;

import java.util.HashMap;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class CLIMessageParserImpl implements MessageParcer<String> {

    public static String INSTRUMENT_TEMPLATE = "<FIELD_NAME_1=VALUE1>;...;<FIELD_NAME_N=VALUE_N>";
    public static String COMMAND_TEMPLATE = "publish <SOURCE> <KEY> " + INSTRUMENT_TEMPLATE;
    public static String COMMAND_EXAMPLE = "publish LME PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=17-03-2018;MARKET=PB;LABEL=Lead 13 March 2018";

    @Override
    public InstrumentUpdate parse(RawMessage<String> message) {
        String rawMessage = message.getMessage().trim();
        int endIndex = 0;
        String action = null, source = null, key = null, body = null;
        //TODO: refactor this place
        try {
            action = rawMessage.substring(endIndex, endIndex = nextIndex(rawMessage, endIndex)).trim();
            source = rawMessage.substring(endIndex, endIndex = nextIndex(rawMessage, endIndex)).trim();
            key = rawMessage.substring(endIndex, endIndex = nextIndex(rawMessage, endIndex)).trim();
            body = rawMessage.substring(endIndex, rawMessage.length()).trim();
        } catch (Exception e) {
            throw new IllegalArgumentException("Command doesn't conform template " + COMMAND_TEMPLATE);
        }

        if (!action.equalsIgnoreCase("publish")) {
            throw new IllegalArgumentException("Expects 'publish' as first word in command");
        }

        HashMap<String, String> instrument = new HashMap<>();
        try {
            String[] keyValues = body.split(";");
            for (String keyValue : keyValues) {
                String[] split = keyValue.split("=");
                instrument.put(split[0], split[1]);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Instrument data should have structure " + INSTRUMENT_TEMPLATE);
        }
        return new InstrumentUpdate(source, key, new Instrument(instrument));
    }

    private int nextIndex(String rawMessage, int endIndex) {
        return rawMessage.indexOf(' ', endIndex+1);
    }

}
