package com.ag.examples.parser;

import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.model.RawMessage;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface MessageParcer<T> {

    InstrumentUpdate parse(RawMessage<T> rawMessage);
}
