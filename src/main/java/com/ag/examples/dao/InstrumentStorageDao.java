package com.ag.examples.dao;

import com.ag.examples.model.Instrument;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface InstrumentStorageDao {

    void put(String key, Instrument instrument);

    Instrument get(String key);

    void clear();
}
