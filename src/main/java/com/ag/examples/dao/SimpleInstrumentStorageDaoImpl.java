package com.ag.examples.dao;

import com.ag.examples.model.Instrument;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class SimpleInstrumentStorageDaoImpl implements InstrumentStorageDao {

    private Map<String, Instrument> instruments = new ConcurrentHashMap<>();

    public void put(String key, Instrument value) {
        instruments.put(key, value);
    }

    public Instrument get(String key) {
        return instruments.get(key);
    }

    @Override
    public void clear() {
        instruments.clear();
    }

    @Override
    public String toString() {
        Set<Map.Entry<String, Instrument>> entries = instruments.entrySet();
        String out = "";
        for (Map.Entry<String, Instrument> entry : entries) {
            out += entry.getKey() + " {" + entry.getValue() + "} \n";
        }
        return out;
    }
}
