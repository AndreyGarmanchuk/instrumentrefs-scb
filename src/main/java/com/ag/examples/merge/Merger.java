package com.ag.examples.merge;

import com.ag.examples.model.Instrument;
import com.ag.examples.dao.InstrumentStorageDao;
import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.policy.MergePolicy;
import com.ag.examples.policy.MergePolicyRepo;

import java.util.Optional;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class Merger {

    private MergePolicyRepo policyRepo;
    private InstrumentStorageDao instrumentStorageDao;

    public Merger(MergePolicyRepo policyRepo, InstrumentStorageDao instrumentStorageDao) {
        this.policyRepo = policyRepo;
        this.instrumentStorageDao = instrumentStorageDao;
    }

    public Instrument merge(InstrumentUpdate instrumentUpdate) {
        MergePolicy mergePolicy = policyRepo.lookupPolicy(instrumentUpdate.getSource());
        String internalKey = mergePolicy.getInternalKey(instrumentUpdate);
        Instrument instrument = instrumentStorageDao.get(internalKey);
        instrument = mergePolicy.merge(Optional.ofNullable(instrument), instrumentUpdate);
        instrumentStorageDao.put(internalKey, instrument);
        return instrument;
    }
}
