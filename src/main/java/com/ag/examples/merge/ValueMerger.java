package com.ag.examples.merge;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface ValueMerger {

    public static enum MergeAction {
        KEY, // this field has value corresponding to internal key, EXCANGE_CODE for PRIME
        OVERRIDE, // replace internal value with updates, add if absed in internal state
        ADD_IF_ABSENT, IGNORE // ignore update for this field
        //DELETE, // fields should be deleted (probabaly makes sense only with some enchanced condition)
        //ADD // add value from update only if value is missed in internal state
    }

    void merge(Instrument instrument, InstrumentUpdate instrumentUpdate);
}
