package com.ag.examples.merge;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

import java.util.function.Predicate;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class PredicateValueMerger implements ValueMerger {

    private String field;
    private ValueMerger.MergeAction trueAction;
    private ValueMerger.MergeAction falseAction;
    private String predicate;

    private Object dummy  = new Object();

    public PredicateValueMerger(String field, String predicate, MergeAction trueAction, MergeAction falseAction) {
        this.field = field;
        this.trueAction = trueAction;
        this.falseAction = falseAction;
        this.predicate = predicate;
    }

    @Override
    public void merge(Instrument instrument, InstrumentUpdate instrumentUpdate) {
        PredicateResolver predicateResolver = new PredicateResolver(predicate, instrument);
        new SimpleValueMerger(field, predicateResolver.test(instrumentUpdate) ? trueAction : falseAction)
                .merge(instrument, instrumentUpdate);
    }

    private static class PredicateResolver implements Predicate<InstrumentUpdate> {
        private String predicate;
        private Instrument instrument;

        public PredicateResolver(String predicate, Instrument instrument) {
            this.predicate = predicate;
            this.instrument = instrument;
        }

        @Override
        public boolean test(InstrumentUpdate instrumentUpdate) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Predicate and(Predicate other) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Predicate negate() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Predicate or(Predicate other) {
            throw new UnsupportedOperationException();
        }
    }
}
