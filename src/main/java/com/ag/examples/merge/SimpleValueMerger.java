package com.ag.examples.merge;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class SimpleValueMerger implements ValueMerger, Comparable {

    private String field;
    private ValueMerger.MergeAction action;
    private String staticValue;

    public SimpleValueMerger(String field, MergeAction action) {
        this.field = field;
        this.action = action;
    }

    public SimpleValueMerger(String tradable, MergeAction action, String staticValue) {
        this(tradable, action);
        this.staticValue = staticValue;
    }

    @Override
    public void merge(Instrument instrument, InstrumentUpdate instrumentUpdate) {
        String newValue = instrumentUpdate.getInstrumentUpdate().get(field);
        if (action == MergeAction.OVERRIDE) {
            instrument.put(field, staticValue != null ? staticValue : newValue);
        }
        if (action == MergeAction.ADD_IF_ABSENT && !instrument.containsKey(field)) {
            instrument.put(field, staticValue != null ? staticValue : newValue);
        }
    }

    @Override
    public String toString() {
        return "rule {" +
                "field='" + field + '\'' +
                ", action=" + action +
                (staticValue == null ? "" : ", staticValue='" + staticValue + '\'') +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleValueMerger that = (SimpleValueMerger) o;

        if (!field.equals(that.field)) return false;
        if (action != that.action) return false;
        boolean result = staticValue != null ? staticValue.equals(that.staticValue) : that.staticValue == null;
        return result;

    }

    @Override
    public int hashCode() {
        int result = field.hashCode();
        result = 31 * result + action.hashCode();
        result = 31 * result + (staticValue != null ? staticValue.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof SimpleValueMerger)) {
            return 0; // ???
        }
        return field.compareTo(((SimpleValueMerger) o).field);
    }

}
