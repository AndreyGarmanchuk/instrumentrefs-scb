package com.ag.examples.policy;

import com.ag.examples.policy.MergePolicy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static com.ag.examples.utils.Logger.logInfo;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class MergePolicyRepo {

    private Map<String, MergePolicy> mergePolicies = new ConcurrentHashMap<>();

    public void registerPolicy(MergePolicy policy) {
        List<String> supportedSources = policy.getSupportedSources();
        for (String supportedSource : supportedSources) {
            if (mergePolicies.containsKey(supportedSource)) {
                logInfo("Overriding policy for " + supportedSource);
            }
            mergePolicies.put(supportedSource, policy);
        }

    }

    public MergePolicy lookupPolicy(String source) {
        if (mergePolicies.size() == 0) {
            throw new IllegalStateException("Policy repository is empty");
        }
        return mergePolicies.get(source);
    }


}
