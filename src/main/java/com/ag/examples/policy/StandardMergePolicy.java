package com.ag.examples.policy;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class StandardMergePolicy implements MergePolicy {

    private static final String LME_SOORCE = "LME";
    private static final String PRIME_SOORCE = "PRIME";

    private static final String EXCHANGE_CODE = "EXCHANGE_CODE";
    private static final String LAST_TRADING_DATE = "LAST_TRADING_DATE";
    private static final String DELIVERY_DATE = "DELIVERY_DATE";
    private static final String MARKET = "MARKET";
    private static final String LABEL = "LABEL";
    private static final String TRADABLE = "TRADABLE";


    private ArrayList<String> supportedSources = new ArrayList<String>() {{
        add(LME_SOORCE);
        add(PRIME_SOORCE);
    }};

    @Override
    public List<String> getSupportedSources() {
        return supportedSources;
    }

    @Override
    public String getInternalKey(InstrumentUpdate instrumentUpdate) {
        if (!supportedSources.contains(instrumentUpdate.getSource())) {
            throw new IllegalArgumentException("Instrument source is not supported for " + instrumentUpdate);
        }

        switch (instrumentUpdate.getSource()) {
            case LME_SOORCE:
                return instrumentUpdate.getKey();
            case PRIME_SOORCE:
                return instrumentUpdate.getInstrumentUpdate().get(EXCHANGE_CODE);
        }

        throw new UnsupportedOperationException("Implementation error for " + instrumentUpdate);
    }

    @Override
    public Instrument merge(Optional<Instrument> instrument, InstrumentUpdate instrumentUpdate) {
        Instrument newInstrument = instrument.isPresent()  ? new Instrument(instrument.get()) : new Instrument();
        if (instrumentUpdate.getSource().equals(LME_SOORCE)) {
            newInstrument.put(LAST_TRADING_DATE, instrumentUpdate.getInstrumentUpdate().get(LAST_TRADING_DATE));
            newInstrument.put(DELIVERY_DATE, instrumentUpdate.getInstrumentUpdate().get(DELIVERY_DATE));
            newInstrument.put(MARKET, instrumentUpdate.getInstrumentUpdate().get(MARKET));
            newInstrument.put(LABEL, instrumentUpdate.getInstrumentUpdate().get(LABEL));
            if (!newInstrument.containsKey(TRADABLE)) {
                newInstrument.put(TRADABLE, "TRUE");
            }
        }

        if (instrumentUpdate.getSource().equals(PRIME_SOORCE)) {
            if (!newInstrument.containsKey(LAST_TRADING_DATE)) {
                newInstrument.put(LAST_TRADING_DATE, instrumentUpdate.getInstrumentUpdate().get(LAST_TRADING_DATE));
            }
            if (!newInstrument.containsKey(DELIVERY_DATE)) {
                newInstrument.put(DELIVERY_DATE, instrumentUpdate.getInstrumentUpdate().get(DELIVERY_DATE));
            }
            if (!newInstrument.containsKey(LABEL)) {
                newInstrument.put(LABEL, instrumentUpdate.getInstrumentUpdate().get(LABEL));
            }

            newInstrument.put(TRADABLE, instrumentUpdate.getInstrumentUpdate().get(TRADABLE));
            newInstrument.put(MARKET, "PB");
        }

        return newInstrument;
    }

}
