package com.ag.examples.policy;

import com.ag.examples.merge.SimpleValueMerger;
import com.ag.examples.merge.ValueMerger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

import static com.ag.examples.utils.Logger.logError;
import static com.ag.examples.utils.Logger.logInfo;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class MergePolicyLoader {

    private MergePolicyRepo policyRepo;
    private String policyFolderPath;

    public static void main(String[] args) throws IOException {
        new MergePolicyLoader(new MergePolicyRepo(), "merge-policies").init();
    }

    public MergePolicyLoader(MergePolicyRepo policyRepo, String policyFolderPath) throws IOException {
        this.policyRepo = policyRepo;
        this.policyFolderPath = policyFolderPath;
    }

    public void init() throws IOException {
        Files.list(Paths.get(policyFolderPath))
                .forEach(path -> loadFile(path));
    }

    private void loadFile(Path path) {
        logInfo("cfg: loading policy from: '" + path + "'...");
        final LoadableMergePolicy policy = new LoadableMergePolicy();
        policy.addSupportedSource(path.getFileName().toString());
        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(line -> {
                line = line.trim();
                if (line.startsWith("#") || line.isEmpty()) {
                    return;
                }
                if (!line.contains(":")) {
                    logError("Line '" + line + "' doesn't conform policy template structure, ignoring...");
                    return;
                }

                String[] split = line.split(":");

                if (split[1].trim().equalsIgnoreCase("KEY")) {
                    policy.setKeyField(split[0].trim());
                    return;
                }

                if (split.length > 2) {
                    policy.addPolicyRule(new SimpleValueMerger(split[0].trim(), ValueMerger.MergeAction.valueOf(split[1].trim()), split[2].trim()));
                } else {
                    policy.addPolicyRule(new SimpleValueMerger(split[0].trim(), ValueMerger.MergeAction.valueOf(split[1].trim())));
                }

            });

        } catch (IOException e) {
            logError("Unexpected exception, file " + path + " is not loaded", e);
        }
        logInfo("cfg: loaded policy: '" + policy);
        policyRepo.registerPolicy(policy);

    }
}
