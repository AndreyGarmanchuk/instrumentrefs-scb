package com.ag.examples.policy;

import com.ag.examples.merge.ValueMerger;
import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

import java.util.*;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class LoadableMergePolicy implements MergePolicy {

    List<String> supportedSource = new ArrayList<>();
    Set<ValueMerger> policyRule = new TreeSet<>();
    String keyField;

    public LoadableMergePolicy() {
    }

    public LoadableMergePolicy(String supportedSource, List<ValueMerger> policyRule, String keyField) {
        this.supportedSource.add(supportedSource);
        this.policyRule.addAll(policyRule);
        this.keyField = keyField;
    }

    public void addSupportedSource(String supportedSource) {
        this.supportedSource.add(supportedSource);
    }

    public void addPolicyRule(ValueMerger policyRule) {
        this.policyRule.add(policyRule);
    }

    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }

    @Override
    public List<String> getSupportedSources() {
        return supportedSource;
    }

    @Override
    public String getInternalKey(InstrumentUpdate instrumentUpdate) {
        if (keyField != null && !keyField.isEmpty()) {
            return instrumentUpdate.getInstrumentUpdate().get(keyField);
        }
        return instrumentUpdate.getKey();
    }

    @Override
    public Instrument merge(Optional<Instrument> instrument, InstrumentUpdate instrumentUpdate) {
        Instrument newInstrument = instrument.isPresent()  ? new Instrument(instrument.get()) : new Instrument();
        for (ValueMerger valueMerger : policyRule) {
            valueMerger.merge(newInstrument, instrumentUpdate);
        }
        return newInstrument;
    }

    @Override
    public String toString() {
        return "{" +
                "supportedSource=" + supportedSource +
                ", keyField='" + keyField + '\'' +
                ", policyRule=" + policyRule +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoadableMergePolicy policy = (LoadableMergePolicy) o;

        if (!supportedSource.equals(policy.supportedSource)) return false;
        if (!policyRule.equals(policy.policyRule)) return false;
        return keyField != null ? keyField.equals(policy.keyField) : policy.keyField == null;

    }

    @Override
    public int hashCode() {
        int result = supportedSource.hashCode();
        result = 31 * result + policyRule.hashCode();
        result = 31 * result + (keyField != null ? keyField.hashCode() : 0);
        return result;
    }
}
