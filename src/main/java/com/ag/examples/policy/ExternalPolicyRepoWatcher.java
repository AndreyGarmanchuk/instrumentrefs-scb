package com.ag.examples.policy;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class ExternalPolicyRepoWatcher {

    public void init() throws IOException, InterruptedException {
        WatchService watcher = FileSystems.getDefault().newWatchService();
        Path mergePoliciesDir = Paths.get("merge-policies");
        mergePoliciesDir.register(watcher,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY);
        boolean b = true;
        while (b) {
            WatchKey watchKey = watcher.take();
            if (watchKey != null) {
                System.out.println();
                List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
                System.out.println("pip " + watchEvents.size());
                WatchEvent<?> event = watchEvents.get(0);
                WatchEvent.Kind<?> kind = event.kind();
                if (kind == OVERFLOW) {
                    continue;
                }
                WatchEvent<Path> ev = (WatchEvent<Path>)event;
                Path filename = ev.context();
                System.out.println(kind.name() + " " + filename);

                // TODO finish processing changes in external repo

                boolean valid = watchKey.reset();
                if (!valid) {
                    break;
                }
            }
        }
    }
}
