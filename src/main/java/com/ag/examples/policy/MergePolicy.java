package com.ag.examples.policy;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;

import java.util.List;
import java.util.Optional;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public interface MergePolicy {

    List<String> getSupportedSources();
    String getInternalKey(InstrumentUpdate instrumentUpdate);
    Instrument merge(Optional<Instrument> instrument, InstrumentUpdate instrumentUpdate);

}
