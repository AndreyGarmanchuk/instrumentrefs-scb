package com.ag.examples;

import com.ag.examples.model.Instrument;
import com.ag.examples.model.InstrumentUpdate;
import com.ag.examples.model.StringRawMessage;
import com.ag.examples.parser.CLIMessageParserImpl;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class CLIMessageParserImplTest {

    private CLIMessageParserImpl cliMessageParser = new CLIMessageParserImpl();

    @Test
    public void okLME() {
        InstrumentUpdate update = cliMessageParser.parse(new StringRawMessage("publish LME PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=17-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018"));
        assertEquals("LME", update.getSource());
        assertEquals("PB_03_2018", update.getKey());
        assertEquals("15-03-2018", update.getInstrumentUpdate().get("LAST_TRADING_DATE"));
        assertEquals("17-03-2018", update.getInstrumentUpdate().get("DELIVERY_DATE"));
        assertEquals("LME_PB", update.getInstrumentUpdate().get("MARKET"));
        assertEquals("Lead 13 March 2018", update.getInstrumentUpdate().get("LABEL"));
    }

    @Test
    public void okPRIME() {
        InstrumentUpdate update = cliMessageParser.parse(new StringRawMessage("publish PRIME PRIME_PB_03_2018 LAST_TRADING_DATE=14-03-2018;DELIVERY_DATE=18-03-2018;MARKET=PB;LABEL=Lead 13 March 2018;EXCHANGE_CODE=PB_03_2018"));
        assertEquals("PRIME", update.getSource());
        assertEquals("PRIME_PB_03_2018", update.getKey());
        assertEquals("14-03-2018", update.getInstrumentUpdate().get("LAST_TRADING_DATE"));
        assertEquals("18-03-2018", update.getInstrumentUpdate().get("DELIVERY_DATE"));
        assertEquals("PB", update.getInstrumentUpdate().get("MARKET"));
        assertEquals("Lead 13 March 2018", update.getInstrumentUpdate().get("LABEL"));
        assertEquals("PB_03_2018", update.getInstrumentUpdate().get("EXCHANGE_CODE"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorWithLackOfParameters() {
        cliMessageParser.parse(new StringRawMessage("publish LME PB_03_2018"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorWithAction() {
        cliMessageParser.parse(new StringRawMessage("LME PB_03_2018"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorWithInstrumentData() {
        cliMessageParser.parse(new StringRawMessage("publish LME PB_03_2018 aaaa"));
    }

}
