package com.ag.examples;

import com.ag.examples.policy.MergePolicy;
import com.ag.examples.policy.MergePolicyLoader;
import com.ag.examples.policy.MergePolicyRepo;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class MergePolicyLoaderTest {

    @Test
    public void testLoading() throws IOException {
        MergePolicyRepo policyRepo = new MergePolicyRepo();
        MergePolicyLoader loader = new MergePolicyLoader(policyRepo, "merge-policies");
        loader.init();

        assertEquals(StaticData.loadableMergePolicyLME, policyRepo.lookupPolicy("LME"));
        assertEquals(StaticData.loadableMergePolicyPRIME, policyRepo.lookupPolicy("PRIME"));
    }
}
