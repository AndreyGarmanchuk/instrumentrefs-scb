package com.ag.examples;

import com.ag.examples.dao.InstrumentStorageDao;
import com.ag.examples.dao.SimpleInstrumentStorageDaoImpl;
import com.ag.examples.merge.Merger;
import com.ag.examples.merge.SimpleValueMerger;
import com.ag.examples.merge.ValueMerger;
import com.ag.examples.model.Instrument;
import com.ag.examples.model.StringRawMessage;
import com.ag.examples.parser.CLIMessageParserImpl;
import com.ag.examples.policy.LoadableMergePolicy;
import com.ag.examples.policy.MergePolicy;
import com.ag.examples.policy.MergePolicyRepo;
import com.ag.examples.policy.StandardMergePolicy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * It is intermediate implementation of Policy, to use it as a reference while developing LoadableMergePolicy
 *
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
@RunWith(Parameterized.class)
public class MergePolicyIntegrationTest {

    private static StandardMergePolicy standardMergePolicy = new StandardMergePolicy();
    public static LoadableMergePolicy loadableMergePolicyLME = StaticData.loadableMergePolicyLME;
    private static LoadableMergePolicy loadableMergePolicyPRIME = StaticData.loadableMergePolicyPRIME;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new ArrayList<MergePolicy>() {{
                    add(standardMergePolicy);
                }}
                },
                {new ArrayList<MergePolicy>() {{
                    add(loadableMergePolicyLME);
                    add(loadableMergePolicyPRIME);
                }}}
        });
    }

    @Parameterized.Parameter(0)
    public List<MergePolicy> mergePolicy;

    private CLIMessageParserImpl cliMessageParser = new CLIMessageParserImpl();
    private MergePolicyRepo mergePolicyRepo = new MergePolicyRepo();
    private InstrumentStorageDao instrumentStorageDao = new SimpleInstrumentStorageDaoImpl();
    private Merger merger = new Merger(mergePolicyRepo, instrumentStorageDao);

    @Before
    public void before() {
        for (MergePolicy policy : mergePolicy) {
            mergePolicyRepo.registerPolicy(policy);
        }
    }

    @After
    public void after() {
        instrumentStorageDao.clear();
    }

    private void publishInstrument(String rawCommand) {
        new MessageProcessingTask<>(new StringRawMessage(rawCommand), cliMessageParser, merger).process();
    }

    @Test
    public void testStory1() {
        publishInstrument("publish LME PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=17-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018");

        Instrument publishedInstrument = instrumentStorageDao.get("PB_03_2018");
        assertEquals("15-03-2018", publishedInstrument.get("LAST_TRADING_DATE"));
        assertEquals("17-03-2018", publishedInstrument.get("DELIVERY_DATE"));
        assertEquals("LME_PB", publishedInstrument.get("MARKET"));
        assertEquals("Lead 13 March 2018", publishedInstrument.get("LABEL"));
        assertEquals("TRUE", publishedInstrument.get("TRADABLE"));
    }

    @Test
    public void testStory2() {
        publishInstrument("publish LME PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=17-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018");
        publishInstrument("publish PRIME PRIME_PB_03_2018 LAST_TRADING_DATE=14-03-2018;DELIVERY_DATE=18-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018;EXCHANGE_CODE=PB_03_2018;TRADABLE=FALSE");

        Instrument publishedInstrument = instrumentStorageDao.get("PB_03_2018");
        assertEquals("15-03-2018", publishedInstrument.get("LAST_TRADING_DATE"));
        assertEquals("17-03-2018", publishedInstrument.get("DELIVERY_DATE"));
        assertEquals("PB", publishedInstrument.get("MARKET"));
        assertEquals("Lead 13 March 2018", publishedInstrument.get("LABEL"));
        assertEquals("FALSE", publishedInstrument.get("TRADABLE"));
    }

    @Test
    public void additionalTestStandardMergePolicyForLME() {
        publishInstrument("publish LME PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=17-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018");
        publishInstrument("publish LME PB_03_2018 LAST_TRADING_DATE=17-03-2018;DELIVERY_DATE=19-03-2018;MARKET=LME_PB;LABEL=Lead 14 March 2018");

        Instrument publishedInstrument = instrumentStorageDao.get("PB_03_2018");
        assertEquals("17-03-2018", publishedInstrument.get("LAST_TRADING_DATE"));
        assertEquals("19-03-2018", publishedInstrument.get("DELIVERY_DATE"));
        assertEquals("LME_PB", publishedInstrument.get("MARKET"));
        assertEquals("Lead 14 March 2018", publishedInstrument.get("LABEL"));
        assertEquals("TRUE", publishedInstrument.get("TRADABLE"));
    }

    /**
     * This test shows that specification a bit ambigous or not fully specific, which can cause unpredicted behaviour.
     */
    @Test
    public void additionalTestStandardMergePolicyForPRIME() {
        publishInstrument("publish PRIME PRIME_PB_03_2018 LAST_TRADING_DATE=14-03-2018;DELIVERY_DATE=18-03-2018;MARKET=LME_PB;LABEL=Lead 13 March 2018;EXCHANGE_CODE=PB_03_2018;TRADABLE=FALSE");
        publishInstrument("publish PRIME PRIME_PB_03_2018 LAST_TRADING_DATE=15-03-2018;DELIVERY_DATE=19-03-2018;MARKET=LME_PB;LABEL=Lead 14 March 2018;EXCHANGE_CODE=PB_03_2018;TRADABLE=TRUE");

        Instrument publishedInstrument = instrumentStorageDao.get("PB_03_2018");
        assertEquals("14-03-2018", publishedInstrument.get("LAST_TRADING_DATE"));
        assertEquals("18-03-2018", publishedInstrument.get("DELIVERY_DATE"));
        assertEquals("Lead 13 March 2018", publishedInstrument.get("LABEL"));
        assertEquals("PB", publishedInstrument.get("MARKET"));
        assertEquals("TRUE", publishedInstrument.get("TRADABLE"));
    }

}
