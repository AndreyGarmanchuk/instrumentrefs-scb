package com.ag.examples;

import com.ag.examples.merge.SimpleValueMerger;
import com.ag.examples.merge.ValueMerger;
import com.ag.examples.policy.LoadableMergePolicy;

import java.util.ArrayList;

/**
 * @author Andrey Garmanchuk (agarmanchuk@gmail.com)
 */
public class StaticData {
    public static LoadableMergePolicy loadableMergePolicyLME = new LoadableMergePolicy("LME",
            new ArrayList<ValueMerger>() {{
                add(new SimpleValueMerger("LAST_TRADING_DATE", ValueMerger.MergeAction.OVERRIDE));
                add(new SimpleValueMerger("DELIVERY_DATE", ValueMerger.MergeAction.OVERRIDE));
                add(new SimpleValueMerger("MARKET", ValueMerger.MergeAction.OVERRIDE));
                add(new SimpleValueMerger("LABEL", ValueMerger.MergeAction.OVERRIDE));
                add(new SimpleValueMerger("TRADABLE", ValueMerger.MergeAction.ADD_IF_ABSENT, "TRUE"));
            }},
            null);

    public static LoadableMergePolicy loadableMergePolicyPRIME = new LoadableMergePolicy("PRIME",
            new ArrayList<ValueMerger>() {{
                add(new SimpleValueMerger("LAST_TRADING_DATE", ValueMerger.MergeAction.ADD_IF_ABSENT));
                add(new SimpleValueMerger("DELIVERY_DATE", ValueMerger.MergeAction.ADD_IF_ABSENT));
                add(new SimpleValueMerger("LABEL", ValueMerger.MergeAction.ADD_IF_ABSENT));
                add(new SimpleValueMerger("TRADABLE", ValueMerger.MergeAction.OVERRIDE));
                add(new SimpleValueMerger("MARKET", ValueMerger.MergeAction.OVERRIDE, "PB"));
            }},
            "EXCHANGE_CODE");

}
