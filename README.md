# Standalone Application for receiving Instruments from Upstream Systems and publishing/storing them internally

## Getting Started
### Prerequisites
1. JRE 1.8 for starting application
2. JDK 1.8 and MAVEN for compilation, testing and packaging

### Installing
To recompile the code and run all test you need to run command

`mvn clean package`


## Running
From application directory you can run either

`java -jar target/instrument-refs-1.0.jar <number_of_processing_threads> <folder-with-rules>`
        
or use 
`START.bat` 
on Windows boxes

When application is started, the following list of commands is available:

|Command 		                                                          |Description                                                        |
|-------------------------------------------------------------------------|-------------------------------------------------------------------|
|publish <SOURCE> <KEY> <FIELD_NAME_1=VALUE1>;...;<FIELD_NAME_N=VALUE_N>  | Should merge, publish and store internaly (TBD)|


## TODO
1. Do not assign message to MessageProcessor at arrivial time. Introduce message queue, and get message from queue only when actual processing is started.
2. Support only string fields in instrument (for future enhancements)
3. External policy repo is stored as set of files in folder. But it would be good to implement watcher (ExternalPolicyRepoWatcher is not finished).
4. Documentation for merge rules